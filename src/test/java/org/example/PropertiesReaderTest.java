package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertiesReaderTest {

    @Test
    void getData() {
        PropertiesReader propertiesReader = new PropertiesReader();
        Data data = propertiesReader.getData();

        assertEquals("Data{min=2, max=5, increment=1, type='null'}", data.toString());
    }
}