package org.example;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * test for main class
 */
class AppTest {

    @Test
    void whenReadTypeIntThenGetListWithIntValues() {
        Data data = new Data("2", "5", "3", "int");
        App app = new App(data);
        List<Object> rsl = app.getRsl();
        List<Integer> trueList = List.of(4, 10, 10, 25);
        List<Integer> notTrueList = List.of(4, 10, 10, 35);
        assertThat(rsl)
                .isEqualTo(trueList)
                .isNotEqualTo(notTrueList);
    }

    @Test
    void whenReadTypeIntThenGetListWithIntValues2() {
        Data data = new Data("2", "5", "2", "int");
        App app = new App(data);
        List<Object> rsl = app.getRsl();
        List<Integer> trueList = List.of(4, 8, 8, 16);
        List<Integer> notTrueList = List.of(4, 10, 10, 35);
        assertThat(rsl)
                .isEqualTo(trueList)
                .isNotEqualTo(notTrueList);
    }

    @Test
    void whenReadTypeDoubleThenGetListWithDoubleValues() throws ParseException {
        Data data = new Data("2.0", "5.0", "3.0", "double");
        App app = new App(data);
        List<Object> rsl = app.getRsl();
        List<Double> trueList = List.of(4.0, 10.0, 10.0, 25.0);
        List<Double> notTrueList = List.of(4.0, 10.0, 10.0, 35.0);
        assertThat(rsl)
                .isEqualTo(trueList)
                .isNotEqualTo(notTrueList);
    }

    @Test
    void whenReadTypeDoubleThenGetListWithDoubleValues2() {
        Data data = new Data("2.0", "5.0", "1.0", "double");
        App app = new App(data);
        List<Object> rsl = app.getRsl();
        List<Double> trueList = List.of(4.0, 6.0, 8.0, 10.0,
                                                6.0, 9.0, 12.0, 15.0,
                                                8.0, 12.0, 16.0, 20.0,
                                                10.0, 15.0, 20.0, 25.0);
        List<Double> notTrueList = List.of(4.0, 10.0, 10.0, 35.0);
        assertThat(rsl)
                .isEqualTo(trueList)
                .isNotEqualTo(notTrueList);
    }
}