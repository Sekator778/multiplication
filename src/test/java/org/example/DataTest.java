package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataTest {

    @Test
    void testToString() {
        Data data = new Data("10", "32", "3");
        assertEquals("Data{min=10, max=32, increment=3, type='null'}", data.toString());
    }
}