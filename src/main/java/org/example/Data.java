package org.example;

public class Data {
    private final String min;
    private final String max;
    private final String increment;
    private String type;

    public Data(String min, String max, String increment) {
        this.min = min;
        this.max = max;
        this.increment = increment;
    }

    public Data(String min, String max, String increment, String type) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMin() {
        return min;
    }

    public String getMax() {
        return max;
    }

    public String getIncrement() {
        return increment;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Data{" +
                "min=" + min +
                ", max=" + max +
                ", increment=" + increment +
                ", type='" + type + '\'' +
                '}';
    }
}
