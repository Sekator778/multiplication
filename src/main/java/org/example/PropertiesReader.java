package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class PropertiesReader {
    /* file name for default */
    private static final String PROPERTIES_FILE_NAME = "app.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesReader.class);
    private final Properties properties;

    public PropertiesReader() {
        properties = new Properties();
        load();
    }

    /**
     * enter point
     */
    private void load() {
        LOGGER.info("properties load ... .");
        System.setProperty("file.encoding", "UTF-8");
        File file = new File("./app.properties");
        if (file.exists()) {
            try (
                    FileInputStream fileInputStream = new FileInputStream(file);
                    InputStreamReader in = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8)
            ) {
                properties.load(in);
                LOGGER.info("properties loading from user file");
            } catch (IOException e) {
                LOGGER.warn("No 'app.property' file {}", e.getMessage());
            }
        } else {
            try {
                properties.load(new InputStreamReader(
                        Objects.requireNonNull(
                                App.class.getClassLoader()
                                        .getResourceAsStream(PROPERTIES_FILE_NAME)),
                        StandardCharsets.UTF_8));
                LOGGER.info("default properties loading");
            } catch (IOException e) {
                LOGGER.warn("No 'app.properties' near jar. So using default file {}", e.getMessage());
            }
        }
        writeToConsole(properties);
    }

    public Data getData() {
        return new Data(properties.getProperty("min", "0"),
                properties.getProperty("max", "0"),
                properties.getProperty("increment", "0"));
    }

    private static void writeToConsole(Properties properties) {
        LOGGER.info("write to Console");
        LOGGER.info("min value {}", properties.getProperty("min"));
        LOGGER.info("max value {}", properties.getProperty("max"));
        LOGGER.info("increment {}", properties.getProperty("increment"));
    }
}