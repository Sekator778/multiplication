package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * This main class
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private List<Object> rsl;
    private Data data;

    public App() {
        init();
        this.rsl = getRsl();
    }

    public App(Data data) {
        this.data = data;
        makeTable();
        this.rsl = getRsl();
    }

    public static void main(String[] args) {
        App app = new App();
        LOGGER.info("work done with result list - {}", app.getRsl());
    }

    private void init() {
        LOGGER.info("App start");
        data = new PropertiesReader().getData();
        Properties systemProperties = System.getProperties();
        LOGGER.info("System type is {}", systemProperties.getProperty("type"));
        data.setType(systemProperties.getProperty("type", "int"));
        LOGGER.info("Data for use {}", data);
        LOGGER.info("make table call");
        this.makeTable();
    }

    private void makeTable() {
        if (rsl == null || rsl.isEmpty()) {
            rsl = new ArrayList<>();
        }
        StringBuilder builder = new StringBuilder(" -> ");
        if (data.getType().equals("int")) {
            LOGGER.info("make grid and fill integer type");
            int i = Integer.parseInt(data.getMin());
            int n = i;
            int increment = Integer.parseInt(data.getIncrement());
            while (i <= Integer.parseInt(data.getMax())) {
                while (n <= Integer.parseInt(data.getMax())) {
                    int mult = i * n;
                    rsl.add(mult);
                    builder.append(mult).append(" ");
                    n += increment;
                }
                i += increment;
                n = Integer.parseInt(data.getMin());
                LOGGER.info("{}", builder);
                builder = new StringBuilder(" -> ");
            }
        } else if (data.getType().equals("double")) {
            LOGGER.info("make grid and fill double type");
            double i = Double.parseDouble(data.getMin());
            double n = i;
            double increment = Double.parseDouble(data.getIncrement());
            while (i <= Double.parseDouble(data.getMax())) {
                while (n <= Double.parseDouble(data.getMax())) {
                    double mult = i * n;
                    rsl.add(mult);
                    builder.append(mult).append(" ");
                    n += increment;
                }
                i += increment;
                n = Double.parseDouble(data.getMin());
                LOGGER.info("{}", builder);
                builder = new StringBuilder(" -> ");
            }
        }
        if (rsl.isEmpty()) {
            LOGGER.error("something wrong {}", data.getType());
            System.exit(-1);
        }
    }

    public List<Object> getRsl() {
        return rsl;
    }
}
